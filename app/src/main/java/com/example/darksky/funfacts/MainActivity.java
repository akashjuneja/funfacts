package com.example.darksky.funfacts;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    public static String TAG = MainActivity.class.getSimpleName();
    private FactBook factBook = new FactBook();
    private colorWheel colorwheel = new colorWheel();
    private TextView textview;
    private Button button;
    private RelativeLayout relativeLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        textview = (TextView) findViewById(R.id.textView);
        button = (Button) findViewById(R.id.button);
        relativeLayout = (RelativeLayout) findViewById(R.id.relative);
        View.OnClickListener onClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String Facts = factBook.getfacts();
                int color = colorwheel.getcolor();
                textview.setText(Facts);
                relativeLayout.setBackgroundColor(color);


            }
        };
        button.setOnClickListener(onClickListener);
        Toast.makeText(this, "first page", Toast.LENGTH_LONG).show();
        Log.d(TAG,"akash");
    }

}



